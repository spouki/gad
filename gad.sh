# Gunzip And Detar
# spouki
# Use it in a base directory for it to go in each directories with .tar.gz archives
# It will gunzip and detar everything

for dir in `ls`
do
    echo "DIR = $dir"
    if [ -d "$dir" ]; then
	cd ./$dir
	for entry in `ls`
	do
	    ext="${entry##*.}"
	    file="${entry%.*}"
	    echo "$file is of type $ext"
	    if [ "$ext" = "gz" ]; then
		echo "gunzipping $file"
		gunzip $entry
		tar -xvf $file
	    fi
	done
	cd ..
    fi
done

exit 1
